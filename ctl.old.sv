module ctl(
    input logic reset, z, INTERRUPT,
    input logic [5:0] opCode,
    input logic [5:0] funct,
    output logic ALUSrc2, RegWrite, MemWrite, MemRead, MemToReg, TAKE_BRANCH, ASel, NOOP,
    output logic [4:0] ALUOp,
    output logic [1:0] RegDst, JUMP
);

    always_comb begin

        if(reset) begin
            RegDst      <= 0;
            ALUSrc2     <= 0;
            RegWrite    <= 0;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp       <= 0;
            JUMP        <= 0;
            TAKE_BRANCH <= 0;
            ASel        <= 0;
        end
        else if(INTERRUPT) begin
            RegDst      <= 3;
            ALUSrc2     <= 0;
            RegWrite    <= 1;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp       <= 26;
            JUMP        <= 0;
            TAKE_BRANCH <= 0;
            ASel        <= 1;
        end
        else if(opCode == 0) begin
            // R type instructions
            RegDst      <= 0;
            ALUSrc2     <= 0;
            RegWrite    <= 1;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            JUMP        <= 0;
            TAKE_BRANCH <= 0;
            ASel        <= 0;
            NOOP        <= 0;

            if(funct == 6'b100000)              // ADD
                ALUOp <= 0;
            else if(funct == 6'b100010)         // SUB
                ALUOp <= 1;
            else if(funct == 6'b100100)         // AND
                ALUOp <= 5'b11000;
            else if(funct == 6'b100111)         // NOR
                ALUOp <= 5'b10001;
            else if(funct == 6'b100101)         // OR
                ALUOp <= 5'b11110;
            else if(funct == 6'b100110)         // XOR
                ALUOp <= 5'b10110;
            else if(funct == 6'b000000) begin   // SLL
                ALUSrc2 <= 1;
                ALUOp   <= 5'b01000;
            end
            else if(funct == 6'b000010) begin   // SRL
                ALUSrc2 <= 1;
                ALUOp   <= 5'b01001;
            end
            else if(funct == 6'b000011) begin   // SRA
                ALUSrc2 <= 1;
                ALUOp   <= 5'b01011;
            end
            else if(funct == 6'b101010)         // SLT
                ALUOp <= 5'b00111;
            else if(funct == 6'b001000) begin   // JR
                RegWrite    <= 0;
                ALUOp       <= 0;
                JUMP        <= 3;
            end
            else begin
                RegDst      <= 3;
                ALUSrc2     <= 0;
                RegWrite    <= 1;
                MemWrite    <= 0;
                MemRead     <= 0;
                MemToReg    <= 0;
                ALUOp       <= 26;
                JUMP        <= 0;
                TAKE_BRANCH <= 0;
                ASel        <= 1;
                NOOP        <= 1;
            end
        end
        else if(opCode == 6'b001000) begin  // ADDI
            RegDst      <= 1;
            ALUSrc2     <= 1;
            RegWrite    <= 1;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp       <= 0;
            JUMP        <= 0;
            TAKE_BRANCH <= 0;
            ASel        <= 0;
            NOOP        <= 0;
        end
        else if(opCode == 6'b001100) begin  // ANDI
            RegDst      <= 1;
            ALUSrc2     <= 1;
            RegWrite    <= 1;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp <= 5'b11000;
            JUMP        <= 0;
            TAKE_BRANCH <= 0;
            ASel        <= 0;
            NOOP        <= 0;
        end
        else if(opCode == 6'b001101) begin  // ORI
            RegDst      <= 1;
            ALUSrc2     <= 1;
            RegWrite    <= 1;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp <= 5'b11110;
            JUMP        <= 0;
            TAKE_BRANCH <= 0;
            ASel        <= 0;
            NOOP        <= 0;
        end
        else if(opCode == 6'b001110) begin  // XORI
            RegDst      <= 1;
            ALUSrc2     <= 1;
            RegWrite    <= 1;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp <= 5'b10110;
            JUMP        <= 0;
            TAKE_BRANCH <= 0;
            ASel        <= 0;
            NOOP        <= 0;
        end
        else if(opCode == 6'b100011) begin  // LW
            RegDst      <= 1;
            ALUSrc2     <= 1;
            RegWrite    <= 1;
            MemWrite    <= 0;
            MemRead     <= 1;
            MemToReg    <= 1;
            ALUOp       <= 0;
            JUMP        <= 0;
            TAKE_BRANCH <= 0;
            ASel        <= 0;
            NOOP        <= 0;
        end
        else if(opCode == 6'b101011) begin  // SW
            RegDst      <= 0;
            ALUSrc2     <= 1;
            RegWrite    <= 0;
            MemWrite    <= 1;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp       <= 0;
            JUMP        <= 0;
            TAKE_BRANCH <= 0;
            ASel        <= 0;
            NOOP        <= 0;
        end
        else if(opCode == 6'b000010) begin  // J
            RegDst      <= 0;
            ALUSrc2     <= 0;
            RegWrite    <= 0;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp       <= 0;
            JUMP        <= 2;
            TAKE_BRANCH <= 0;
            ASel        <= 0;
            NOOP        <= 0;
        end
        else if(opCode == 6'b000011) begin  // JAL
            RegDst      <= 2;
            ALUSrc2     <= 0;
            RegWrite    <= 1;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp       <= 26;
            JUMP        <= 2;
            TAKE_BRANCH <= 0;
            ASel        <= 1;
            NOOP        <= 0;
        end
        else if(opCode == 6'b000100) begin  // BEQ
            RegDst      <= 0;
            ALUSrc2     <= 0;
            RegWrite    <= 0;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp       <= 1;//5;
            JUMP        <= 0;
            TAKE_BRANCH <= z;
            ASel        <= 0;
            NOOP        <= 0;
        end
        else if(opCode == 6'b000101) begin  // BNE
            RegDst      <= 0;
            ALUSrc2     <= 0;
            RegWrite    <= 0;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp       <= 1;//5;
            JUMP        <= 0;
            TAKE_BRANCH <= !z;
            ASel        <= 0;
            NOOP        <= 0;
        end
        else begin
            RegDst      <= 3;
            ALUSrc2     <= 0;
            RegWrite    <= 1;
            MemWrite    <= 0;
            MemRead     <= 0;
            MemToReg    <= 0;
            ALUOp       <= 26;
            JUMP        <= 0;
            TAKE_BRANCH <= 0;
            ASel        <= 1;
            NOOP        <= 1;
        end
    end

endmodule
