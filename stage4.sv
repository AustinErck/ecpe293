module stage4(
    input logic clk, MEM_WRITE, MEM_READ,
    input logic [31:0] aluOut, regDataB,
    output logic [31:0] memData
);

    /*** Internal Signals ***/
    /*** Signal Assignment ***/


    /*** Modules ***/

    dmem dutDmem(
        // Inputs
        .clk(clk),
        .MemWrite(MEM_WRITE),
        .MemRead(MEM_READ),
        .memAddr(aluOut),
        .memWriteData(regDataB),
        // Outputs
        .memReadData(memData)
    );

endmodule