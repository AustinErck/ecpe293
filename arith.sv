module fullAdder(
	input logic a, b, cin,
	output logic sum, cout 
);

	// Combine inputs
	logic [2:0] in;
	assign in[2] = a;
	assign in[1] = b;
	assign in[0] = cin;

	always_comb begin
		case (in)
			3'b111: begin
				cout <= 1'b1;
				sum <= 1'b1;
			end
			3'b001, 3'b010, 3'b100: begin
				cout <= 1'b0;
				sum <= 1'b1;
			end
			3'b011, 3'b101, 3'b110: begin
				cout <= 1'b1;
				sum <= 1'b0;
			end
			default: begin
				cout <= 1'b0;
				sum <= 1'b0;
			end
		endcase
	end

endmodule

module arith(
	input logic [1:0] ALUOp,
	input logic [31:0] A, B,
    output logic [31:0] arithout,
	output logic z, v, n
);

	logic [31:0] formattedB, carry;

	assign formattedB = (ALUOp[0] == 1'b1) ? (~B + 1) : B;

	fullAdder bit31(.a(A[31]), .b(formattedB[31]), .cin(carry[30]), .sum(arithout[31]), .cout(carry[31]));
	fullAdder bit30(.a(A[30]), .b(formattedB[30]), .cin(carry[29]), .sum(arithout[30]), .cout(carry[30]));
	fullAdder bit29(.a(A[29]), .b(formattedB[29]), .cin(carry[28]), .sum(arithout[29]), .cout(carry[29]));
	fullAdder bit28(.a(A[28]), .b(formattedB[28]), .cin(carry[27]), .sum(arithout[28]), .cout(carry[28]));
	fullAdder bit27(.a(A[27]), .b(formattedB[27]), .cin(carry[26]), .sum(arithout[27]), .cout(carry[27]));
	fullAdder bit26(.a(A[26]), .b(formattedB[26]), .cin(carry[25]), .sum(arithout[26]), .cout(carry[26]));
	fullAdder bit25(.a(A[25]), .b(formattedB[25]), .cin(carry[24]), .sum(arithout[25]), .cout(carry[25]));
	fullAdder bit24(.a(A[24]), .b(formattedB[24]), .cin(carry[23]), .sum(arithout[24]), .cout(carry[24]));
	fullAdder bit23(.a(A[23]), .b(formattedB[23]), .cin(carry[22]), .sum(arithout[23]), .cout(carry[23]));
	fullAdder bit22(.a(A[22]), .b(formattedB[22]), .cin(carry[21]), .sum(arithout[22]), .cout(carry[22]));
	fullAdder bit21(.a(A[21]), .b(formattedB[21]), .cin(carry[20]), .sum(arithout[21]), .cout(carry[21]));
	fullAdder bit20(.a(A[20]), .b(formattedB[20]), .cin(carry[19]), .sum(arithout[20]), .cout(carry[20]));
	fullAdder bit19(.a(A[19]), .b(formattedB[19]), .cin(carry[18]), .sum(arithout[19]), .cout(carry[19]));
	fullAdder bit18(.a(A[18]), .b(formattedB[18]), .cin(carry[17]), .sum(arithout[18]), .cout(carry[18]));
	fullAdder bit17(.a(A[17]), .b(formattedB[17]), .cin(carry[16]), .sum(arithout[17]), .cout(carry[17]));
	fullAdder bit16(.a(A[16]), .b(formattedB[16]), .cin(carry[15]), .sum(arithout[16]), .cout(carry[16]));
	fullAdder bit15(.a(A[15]), .b(formattedB[15]), .cin(carry[14]), .sum(arithout[15]), .cout(carry[15]));
	fullAdder bit14(.a(A[14]), .b(formattedB[14]), .cin(carry[13]), .sum(arithout[14]), .cout(carry[14]));
	fullAdder bit13(.a(A[13]), .b(formattedB[13]), .cin(carry[12]), .sum(arithout[13]), .cout(carry[13]));
	fullAdder bit12(.a(A[12]), .b(formattedB[12]), .cin(carry[11]), .sum(arithout[12]), .cout(carry[12]));
	fullAdder bit11(.a(A[11]), .b(formattedB[11]), .cin(carry[10]), .sum(arithout[11]), .cout(carry[11]));
	fullAdder bit10(.a(A[10]), .b(formattedB[10]), .cin(carry[9]), .sum(arithout[10]), .cout(carry[10]));
	fullAdder bit9(.a(A[9]), .b(formattedB[9]), .cin(carry[8]), .sum(arithout[9]), .cout(carry[9]));
	fullAdder bit8(.a(A[8]), .b(formattedB[8]), .cin(carry[7]), .sum(arithout[8]), .cout(carry[8]));
	fullAdder bit7(.a(A[7]), .b(formattedB[7]), .cin(carry[6]), .sum(arithout[7]), .cout(carry[7]));
	fullAdder bit6(.a(A[6]), .b(formattedB[6]), .cin(carry[5]), .sum(arithout[6]), .cout(carry[6]));
	fullAdder bit5(.a(A[5]), .b(formattedB[5]), .cin(carry[4]), .sum(arithout[5]), .cout(carry[5]));
	fullAdder bit4(.a(A[4]), .b(formattedB[4]), .cin(carry[3]), .sum(arithout[4]), .cout(carry[4]));
	fullAdder bit3(.a(A[3]), .b(formattedB[3]), .cin(carry[2]), .sum(arithout[3]), .cout(carry[3]));
	fullAdder bit2(.a(A[2]), .b(formattedB[2]), .cin(carry[1]), .sum(arithout[2]), .cout(carry[2]));
	fullAdder bit1(.a(A[1]), .b(formattedB[1]), .cin(carry[0]), .sum(arithout[1]), .cout(carry[1]));
	fullAdder bit0(.a(A[0]), .b(formattedB[0]), .cin(1'b0), .sum(arithout[0]), .cout(carry[0]));

	assign z = arithout == 0;
	assign v = A[31] == formattedB[31] && A[31] != arithout[31];
	assign n = arithout[31];

endmodule