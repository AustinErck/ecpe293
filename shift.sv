module shift(
	input logic [1:0] ALUOp,
	input logic signed [31:0] A, 
	input logic [31:0] B,
	output logic [31:0] shiftout
);

	always_comb begin
		if(ALUOp == 0) begin //SLL
			shiftout <= A << B[4:0];
		end
		else if(ALUOp == 1) begin //SRL
			shiftout <= A >> B[4:0];
		end
		else if(ALUOp == 3) begin //SRA
			shiftout <= A >>> B[4:0];
		end
		else begin
			shiftout <= 0;
		end
	end

endmodule