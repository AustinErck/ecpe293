module beta(
    input logic clk, reset, IRQ,
    output logic [31:0] instrAddr, memAddr,
    output logic MEM_READ, MEM_WRITE
);     

    // Declare backwards facing signals
    logic NOOP_s2;
    logic [4:0] regAddrW_s4, regAddrW_s5;
    logic [31:0] aluOut_s4, regDataW_s5;

    /*** Stage 1 ***/

    // Signals
    logic [31:0] instrAddr_s1, instrData_s1, nextInstrAddr_s1;

    // Testing Assignments
    assign instrAddr = instrAddr_s1;

    stage1 STAGE1(
        // Inputs
        .clk(clk),
        .reset(reset),
        .IRQ(IRQ),
        .NOOP(NOOP_s2),
        .STALL(STALL),
        // Outputs
        .instrAddr(instrAddr_s1),
        .instrData(instrData_s1),
        .nextInstrAddr(nextInstrAddr_s1)
    );


    /*** Stage 2 ***/

    // Control signals
    logic REG_WRITE_s2, ALU_SRC1_s2, ALU_SRC2_s2, MEM_READ_s2, MEM_WRITE_s2, MEM_REG_s2;
    logic [1:0] REG_DEST_s2;
    logic [4:0] ALU_OP_s2; 

    // Signals
    logic [4:0] regAddrA_s2, regAddrB_s2, regAddrC_s2, regAddrB_s3;
    logic [5:0] opCode_s2, opCode_s3, funct_s2;
    logic [15:0] immediateAddr;
    logic [25:0] targetAddress_s2;
    logic [31:0] nextInstrAddr_s2, instrData_s2, regDataA_s2, regDataB_s2, shamtZero_s2, immediateSignExt_s2, immediateZero_s2;

    // Pipeline forwarding IF -> ID
    always_ff @(posedge clk) begin
        if(!STALL) begin
            nextInstrAddr_s2 <= nextInstrAddr_s1;
            instrData_s2 <= instrData_s1;
        end
    end
    
    // Testing Assignments
    assign MEM_WRITE = MEM_WRITE_s2;

    stage2 STAGE2(
        // Inputs
        .clk(clk),
        .REG_WRITE(REG_WRITE_s2),
        .REG_DEST(REG_DEST_s2),
        .instrData(instrData_s2),
        .regAddrW(regAddrW_s5),
        .regDataW(regDataW_s5),
        // Outputs
        .opCode(opCode_s2),
        .funct(funct_s2),
        .regAddrA(regAddrA_s2),
        .regAddrB(regAddrB_s2), 
        .regAddrC(regAddrC_s2),
        .targetAddress(targetAddress_s2),
        .regDataA(regDataA_s2),
        .regDataB(regDataB_s2),
        .shamtZero(shamtZero_s2),
        .immediateSignExt(immediateSignExt_s2),
        .immediateZero(immediateZero_s2)
    );

    ctl CONTROL(
        // Inputs
        .reset(reset),
        .opCode(opCode_s2),
        .funct(funct_s2),
        .IRQ(IRQ),
        .instrAddr(instrData_s2),
        // Outputs
        .REG_WRITE(REG_WRITE_s2),
        .ALU_SRC1(ALU_SRC1_s2),
        .ALU_SRC2(ALU_SRC2_s2),
        .MEM_READ(MEM_READ_s2),
        .MEM_WRITE(MEM_WRITE_s2),
        .MEM_REG(MEM_REG_s2),
        .NOOP(NOOP_s2),
        .REG_DEST(REG_DEST_s2),
        .ALU_OP(ALU_OP_s2)
    );

    hazardDetection HAZARD(
        // Inputs
        .clk(clk),
        .regAddrA_s2(regAddrA_s2),
        .regAddrB_s2(regAddrB_s2),
        .regAddrB_s3(regAddrB_s3),
        .opCode_s2(opCode_s2),
        .opCode_s3(opCode_s3),
        // Outputs
        .STALL(STALL)
    );


    /*** Stage 3 ***/

    // Control signals
    logic ALU_SRC1_s3, ALU_SRC2_s3, MEM_READ_s3, MEM_WRITE_s3, MEM_REG_s3;
    logic [1:0] REG_DEST_s3, FWD_SRC1, FWD_SRC2;
    logic [4:0] ALU_OP_s3; 

    // Signals
    logic isZero_s3, causedOverflow_s3, isNegative_s3;
    logic [4:0] regAddrA_s3, regAddrC_s3;
    logic [25:0] targetAddress_s3;
    logic [31:0] instrData_s3, nextInstrAddr_s3, regDataA_s3, regDataB_s3, fwdRegDataB_s3, shamtZero_s3, immediateSignExt_s3, immediateZero_s3, regAddrW_s3, aluOut_s3;

    // Testing Assignments
    assign MEM_READ = MEM_READ_s3;

    // Pipeline forwarding ID -> EX
    always_ff @(posedge clk) begin
        //if(!STALL) begin
            ALU_SRC1_s3 <= ALU_SRC1_s2;
            ALU_SRC2_s3 <= ALU_SRC2_s2;
            MEM_READ_s3 <= MEM_READ_s2;
            MEM_WRITE_s3 <= MEM_WRITE_s2;
            MEM_REG_s3 <= MEM_REG_s2;
            REG_DEST_s3 <= REG_DEST_s2;
            ALU_OP_s3 <= ALU_OP_s2;
            opCode_s3 <= opCode_s2;
            regAddrA_s3 <= regAddrA_s2;
            regAddrB_s3 <= regAddrB_s2;
            regAddrC_s3 <= regAddrC_s2;
            targetAddress_s3 <= targetAddress_s2;
            instrData_s3 <= instrData_s2;
            nextInstrAddr_s3 <= nextInstrAddr_s2;
            regDataA_s3 <= regDataA_s2;
            regDataB_s3 <= regDataB_s2;
            shamtZero_s3 <= shamtZero_s2;
            immediateSignExt_s3 <= immediateSignExt_s2;
            immediateZero_s3 <= immediateZero_s2;
        //end
        //else begin
            //MEM_READ_s3 <= 0;
            //MEM_WRITE_s3 <= 0;
        //end
    end
    
    // Testing Assignments
    assign memAddr = aluOut_s3;

    stage3 STAGE3(
        // Inputs
        .ALU_OP(ALU_OP_s3),
        .ALU_SRC1(ALU_SRC1_s3),
        .ALU_SRC2(ALU_SRC2_s3),
        .FWD_SRC1(FWD_SRC1), 
        .FWD_SRC2(FWD_SRC2), 
        .opCode(opCode_s3),
        //.targetAddress(targetAddress_s2), BRANCH ONLY
        .nextInstrAddr(nextInstrAddr_s3),
        .regDataA(regDataA_s3),
        .regDataB(regDataB_s3),
        .regDataW(regDataW_s5),
        .aluOutPrev(aluOut_s4),
        .shamtZero(shamtZero_s3),
        .immediateSignExt(immediateSignExt_s3),
        .immediateZero(immediateZero_s3),
        // Outputs
        .isZero(isZero_s3),
        .causedOverflow(causedOverflow_s3),
        .isNegative(isNegative_s3),
        .fwdRegDataB(fwdRegDataB_s3),
        .aluOut(aluOut_s3)
    );

    fwd FORWARD(
        // Inputs
        .regAddrA_ex(regAddrA_s3),
        .regAddrB_ex(regAddrB_s3),
        .regAddrW_mem(regAddrW_s4),
        .regAddrW_wb(regAddrW_s5),
        // Outputs
        .FWD_SRC1(FWD_SRC1),
        .FWD_SRC2(FWD_SRC2)
    );

    // Determine register write address
    always_comb begin
    
        if(REG_DEST_s3) begin
            regAddrW_s3 <= regAddrB_s3;
        end
        else begin
            regAddrW_s3 <= regAddrC_s3;
        end
    end


    /*** Stage 4 ***/

    // Control signals
    logic MEM_READ_s4, MEM_WRITE_s4, MEM_REG_s4;

    // Signals
    logic [31:0] instrData_s4, regDataB_s4, memData_s4;

    // Pipeline forwarding IF -> ID
    always_ff @(posedge clk) begin
    	MEM_READ_s4 <= MEM_READ_s3;
        MEM_WRITE_s4 <= MEM_WRITE_s3;
        MEM_REG_s4 <= MEM_REG_s3;
        instrData_s4 <= instrData_s3;
        aluOut_s4 <= aluOut_s3;
        regDataB_s4 <= regDataB_s3;
        regAddrW_s4 <= regAddrW_s3;
    end

    stage4 STAGE4(
        // Inputs
        .clk(clk),
        .MEM_READ(MEM_READ_s4),
        .MEM_WRITE(MEM_WRITE_s4),
        .aluOut(aluOut_s4),
        .regDataB(fwdRegDataB_s3),
        // Outputs
        .memData(memData_s4)
    );


    /*** Stage 5 ***/

    // Control signals
    logic MEM_REG_s5;

    // Signals
    logic [31:0] memData_s5, aluOut_s5;

    // Pipeline forwarding IF -> ID
    always_ff @(posedge clk) begin
    	MEM_REG_s5 <= MEM_REG_s4;
        memData_s5 <= memData_s4;
        aluOut_s5 <= aluOut_s4;
        regAddrW_s5 <= regAddrW_s4;
    end

    // Stage 5 is simple 2:1 mux
    always_comb begin
        if(MEM_REG_s5) begin
            regDataW_s5 <= memData_s5;
        end
        else begin
            regDataW_s5 <= aluOut_s5;
        end
    end

endmodule