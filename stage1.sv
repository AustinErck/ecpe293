module stage1(
	input logic clk, reset, IRQ, NOOP, STALL,
    output logic [31:0] instrAddr, instrData, nextInstrAddr
);

    /*** Modules ***/

    pc programCounter(
        // Inputs
        .clk(clk),
        .reset(reset),
        .IRQ(IRQ),
        .NOOP(NOOP),
        .STALL(STALL),
        // Outputs
        .instrAddr(instrAddr),
        .nextInstrAddr(nextInstrAddr)
    );

    imem dutImem(
        // Inputs
        .clk(clk),
        .ia(instrAddr),
        // Outputs
        .id(instrData)
    );


endmodule