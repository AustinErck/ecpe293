module stage3(
    input logic ALU_SRC1, ALU_SRC2,
    input logic [1:0] FWD_SRC1, FWD_SRC2,
    input logic [4:0] ALU_OP,
    input logic [5:0] opCode,
    //input logic [25:0] targetAddress, BRANCH ONLY
    input logic [31:0] nextInstrAddr, regDataA, regDataB, regDataW, aluOutPrev, shamtZero, immediateSignExt, immediateZero,
    output logic isZero, causedOverflow, isNegative,
    output logic [31:0] fwdRegDataB, aluOut
);

    /*** Internal Signals ***/
    
    logic [31:0] aluInA, aluInB;


    /*** Signal Assignment ***/

    // Determine aluInA
    always_comb begin
        if(FWD_SRC1 == 3) begin
            aluInA <= regDataW;
        end
        else if(FWD_SRC1 == 2) begin
            aluInA <= aluOutPrev;
        end
        else begin 
            aluInA <= regDataA;
        end
    end

    // Determine fwdRegDataB
    always_comb begin
        if(FWD_SRC2 == 3) begin
            fwdRegDataB <= regDataW;
        end
        else if(FWD_SRC2 == 2) begin
            fwdRegDataB <= aluOutPrev;
        end
        else begin 
            fwdRegDataB <= regDataB;
        end
    end

    // Determine aluInB
    always_comb begin
        if(!ALU_SRC2) begin
            aluInB <= fwdRegDataB;
        end
        else if(opCode == 0) begin // R types might need zero padded shamt
            aluInB <= shamtZero;
        end
        else if(opCode == 6'b100011 || opCode == 6'b101011 || ALU_OP[4]) begin // Zero padded immediate is used for boolean operations and load/store word
            aluInB <= immediateZero;
        end
        else begin // Sign extended is used everywhere else
            aluInB <= immediateSignExt;
        end
    end
    

    /*** Modules ***/

    alu ALU(
        // Inputs
        .A(aluInA),
        .B(aluInB),
        .ALUOp(ALU_OP),
        // Outputs
        .Y(aluOut),
        .z(isZero),
        .v(causedOverflow),
        .n(isNegative)
    );

endmodule