module fwd(
    input logic [4:0] regAddrA_ex, regAddrB_ex, regAddrW_mem, regAddrW_wb,
    output logic [1:0] FWD_SRC1, FWD_SRC2
);

    // Determine FWD_SRC1
    always_comb begin
        if(regAddrA_ex == regAddrW_mem) begin
            FWD_SRC1 <= 2;
        end
        else if(regAddrA_ex == regAddrW_wb) begin
            FWD_SRC1 <= 3;
        end
        else begin 
            FWD_SRC1 <= 0;
        end
    end

    // Determine FWD_SRC2
    always_comb begin
        if(regAddrB_ex == regAddrW_mem) begin
            FWD_SRC2 <= 2;
        end
        else if(regAddrB_ex == regAddrW_wb) begin
            FWD_SRC2 <= 3;
        end
        else begin 
            FWD_SRC2 <= 0;
        end
    end

endmodule
