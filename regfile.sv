module regfile(
	input logic clk, RegWrite,
    input logic [1:0] RegDst,
    input logic [4:0] ra, rb, rc,
    input logic [31:0] wdata,
    output logic [31:0] radata, rbdata
);

    // Create registers
	logic [31:0] registers[31:0];
	
    // Determine write register based on control signal
	logic [4:0] rd;
     always_comb begin
        if(RegDst == 3)
            rd <= 1;
        else if(RegDst == 2) 
            rd <= 31;
        else if(RegDst == 1)
            rd <= rb;
        else
            rd <= rc;
    end

    // Assign registers as always read
    assign radata = registers[ra];
    assign rbdata = registers[rb];

    // Set initial register values
    initial begin
        $readmemh("registers.mif", registers);
    end

    // Read/write on positive edge of clock
	always_ff @(negedge clk) begin

        // Write data if control signal is set (Prevent writes to $zero)
        if(RegWrite && rd != 0  && (rd != 1 || (rd == 1 && RegDst == 3 ))) begin
            registers[rd] <= wdata;
        end 
    end

endmodule