onerror {resume}
radix define OP_LABELS {
    "6'b000000" "RType",
    "6'b000001" "NOP",
    "6'b000010" "J",
    "6'b000011" "JAL",
    "6'b000100" "BEQ",
    "6'b000101" "BNE",
    "6'b000110" "NOP",
    "6'b000111" "NOP",
    "6'b001000" "ADDI",
    "6'b001001" "NOP",
    "6'b001010" "NOP",
    "6'b001011" "NOP",
    "6'b001100" "ANDI",
    "6'b001101" "ORI",
    "6'b001110" "XORI",
    "6'b001111" "NOP",
    "6'b010000" "NOP",
    "6'b010001" "NOP",
    "6'b010010" "NOP",
    "6'b010011" "NOP",
    "6'b010100" "NOP",
    "6'b010101" "NOP",
    "6'b010110" "NOP",
    "6'b010111" "NOP",
    "6'b011000" "NOP",
    "6'b011001" "NOP",
    "6'b011010" "NOP",
    "6'b011011" "NOP",
    "6'b011100" "NOP",
    "6'b011101" "NOP",
    "6'b011110" "NOP",
    "6'b011111" "NOP",
    "6'b100000" "NOP",
    "6'b100001" "NOP",
    "6'b100010" "NOP",
    "6'b100011" "LW",
    "6'b100100" "NOP",
    "6'b100101" "NOP",
    "6'b100110" "NOP",
    "6'b100111" "NOP",
    "6'b101000" "NOP",
    "6'b101001" "NOP",
    "6'b101010" "NOP",
    "6'b101011" "SW",
    "6'b101100" "NOP",
    "6'b101101" "NOP",
    "6'b101110" "NOP",
    "6'b101111" "NOP",
    "6'b110000" "NOP",
    "6'b110001" "NOP",
    "6'b110010" "NOP",
    "6'b110011" "NOP",
    "6'b110100" "NOP",
    "6'b110101" "NOP",
    "6'b110110" "NOP",
    "6'b110111" "NOP",
    "6'b111000" "NOP",
    "6'b111001" "NOP",
    "6'b111010" "NOP",
    "6'b111011" "NOP",
    "6'b111100" "NOP",
    "6'b111101" "NOP",
    "6'b111110" "NOP",
    "6'b111111" "NOP",
    -default binary
}
radix define FUNC_LABELS {
    "6'b000000" "SLL",
    "6'b000001" "NOP",
    "6'b000010" "SRL",
    "6'b000011" "SRA",
    "6'b000100" "NOP",
    "6'b000101" "NOP",
    "6'b000110" "NOP",
    "6'b000111" "NOP",
    "6'b001000" "JR",
    "6'b001001" "NOP",
    "6'b001010" "NOP",
    "6'b001011" "NOP",
    "6'b001100" "NOP",
    "6'b001101" "NOP",
    "6'b001110" "NOP",
    "6'b001111" "NOP",
    "6'b010000" "NOP",
    "6'b010001" "NOP",
    "6'b010010" "NOP",
    "6'b010011" "NOP",
    "6'b010100" "NOP",
    "6'b010101" "NOP",
    "6'b010110" "NOP",
    "6'b010111" "NOP",
    "6'b011000" "NOP",
    "6'b011001" "NOP",
    "6'b011010" "NOP",
    "6'b011011" "NOP",
    "6'b011100" "NOP",
    "6'b011101" "NOP",
    "6'b011110" "NOP",
    "6'b011111" "NOP",
    "6'b100000" "ADD",
    "6'b100001" "NOP",
    "6'b100010" "SUB",
    "6'b100011" "NOP",
    "6'b100100" "AND",
    "6'b100101" "OR",
    "6'b100110" "XOR",
    "6'b100111" "NOR",
    "6'b101000" "NOP",
    "6'b101001" "NOP",
    "6'b101010" "SLT",
    "6'b101011" "NOP",
    "6'b101100" "NOP",
    "6'b101101" "NOP",
    "6'b101110" "NOP",
    "6'b101111" "NOP",
    "6'b110000" "NOP",
    "6'b110001" "NOP",
    "6'b110010" "NOP",
    "6'b110011" "NOP",
    "6'b110100" "NOP",
    "6'b110101" "NOP",
    "6'b110110" "NOP",
    "6'b110111" "NOP",
    "6'b111000" "NOP",
    "6'b111001" "NOP",
    "6'b111010" "NOP",
    "6'b111011" "NOP",
    "6'b111100" "NOP",
    "6'b111101" "NOP",
    "6'b111110" "NOP",
    "6'b111111" "NOP",
    -default binary
}
radix define REG_LABELS {
    "5'b00000" "R0",
    "5'b00001" "R1",
    "5'b00010" "R2",
    "5'b00011" "R3",
    "5'b00100" "R4",
    "5'b00101" "R5",
    "5'b00110" "R6",
    "5'b00111" "R7",
    "5'b01000" "R8",
    "5'b01001" "R9",
    "5'b01010" "R10",
    "5'b01011" "R11",
    "5'b01100" "R12",
    "5'b01101" "R13",
    "5'b01110" "R14",
    "5'b01111" "R15",
    "5'b10000" "R16",
    "5'b10001" "R17",
    "5'b10010" "R18",
    "5'b10011" "R19",
    "5'b10100" "R20",
    "5'b10101" "R21",
    "5'b10110" "R22",
    "5'b10111" "R23",
    "5'b11000" "R24",
    "5'b11001" "R25",
    "5'b11010" "R26",
    "5'b11011" "R27",
    "5'b11100" "R28",
    "5'b11101" "R29",
    "5'b11110" "R30",
    "5'b11111" "R31",
    -default binary
}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label Clk /testBeta/clk
add wave -noupdate -label Reset /testBeta/reset
add wave -noupdate -label IRQ /testBeta/irq
add wave -noupdate -divider Stage=IF
add wave -noupdate -label IA_IF -radix hexadecimal /testBeta/ia
add wave -noupdate -label OpCodeIF -radix OP_LABELS {/testBeta/dutBeta/instrData_s1[31:26]}
add wave -noupdate -divider Stage=ID
add wave -noupdate -label OpCodeID -radix OP_LABELS {/testBeta/dutBeta/instrData_s2[31:26]}
add wave -noupdate -label FunctID -radix FUNC_LABELS {/testBeta/dutBeta/instrData_s2[5:0]}
add wave -noupdate -label RsID -radix REG_LABELS {/testBeta/dutBeta/instrData_s2[25:21]}
add wave -noupdate -label RtID -radix REG_LABELS {/testBeta/dutBeta/instrData_s2[20:16]}
add wave -noupdate -label RdID -radix REG_LABELS {/testBeta/dutBeta/instrData_s2[15:11]}
add wave -noupdate -divider Stage=EX
add wave -noupdate -label OpCodeEX -radix OP_LABELS {/testBeta/dutBeta/instrData_s3[31:26]}
add wave -noupdate -label FunctEX -radix FUNC_LABELS {/testBeta/dutBeta/instrData_s3[5:0]}
add wave -noupdate -label RsEX -radix REG_LABELS {/testBeta/dutBeta/instrData_s3[25:21]}
add wave -noupdate -label RtEX -radix REG_LABELS {/testBeta/dutBeta/instrData_s3[20:16]}
add wave -noupdate -label RdEX -radix REG_LABELS {/testBeta/dutBeta/instrData_s3[15:11]}
add wave -noupdate -label MemAddrEX -radix hexadecimal /testBeta/memAddr
add wave -noupdate -divider Stage=MEM
add wave -noupdate -label OpCodeMEM -radix OP_LABELS {/testBeta/dutBeta/instrData_s4[31:26]}
add wave -noupdate -label FunctMEM -radix FUNC_LABELS {/testBeta/dutBeta/instrData_s4[5:0]}
add wave -noupdate -label RsMEM -radix REG_LABELS {/testBeta/dutBeta/instrData_s4[25:21]}
add wave -noupdate -label RtMEM -radix REG_LABELS {/testBeta/dutBeta/instrData_s4[20:16]}
add wave -noupdate -label RdMEM -radix REG_LABELS {/testBeta/dutBeta/instrData_s4[15:11]}
add wave -noupdate -divider Debug_Signals
add wave -noupdate /testBeta/dutBeta/STALL
add wave -noupdate -radix hexadecimal /testBeta/clk
add wave -noupdate -radix hexadecimal /testBeta/reset
add wave -noupdate -radix hexadecimal /testBeta/irq
add wave -noupdate -radix hexadecimal /testBeta/ia
add wave -noupdate -radix hexadecimal /testBeta/memAddr
add wave -noupdate -radix hexadecimal /testBeta/MemWrite
add wave -noupdate -radix hexadecimal /testBeta/MemRead
add wave -noupdate -radix hexadecimal /testBeta/cntlIn
add wave -noupdate -radix hexadecimal /testBeta/iaExpected
add wave -noupdate -radix hexadecimal /testBeta/memAddrExpected
add wave -noupdate -radix hexadecimal /testBeta/memWriteDataExpected
add wave -noupdate -radix hexadecimal /testBeta/MemWriteExpected
add wave -noupdate -radix hexadecimal /testBeta/MemReadExpected
add wave -noupdate -radix hexadecimal /testBeta/inputIndex
add wave -noupdate -radix hexadecimal /testBeta/checkIAIndex
add wave -noupdate -radix hexadecimal /testBeta/checkMemAddrIndex
add wave -noupdate -radix hexadecimal /testBeta/dutBeta/STAGE2/registers/registers
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {490 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 137
configure wave -valuecolwidth 120
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {579 ps}
