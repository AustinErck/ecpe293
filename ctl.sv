module ctl(
    input logic reset, IRQ,
    input logic [5:0] opCode, funct,
    input logic [31:0] instrAddr,
    output logic REG_WRITE, ALU_SRC1, ALU_SRC2, MEM_READ, MEM_WRITE, MEM_REG, NOOP,
    output logic [1:0] REG_DEST,
    output logic [4:0] ALU_OP
);

    /*** Internal Signals ***/

    logic INTERRUPT;


    /*** Signal Assignment ***/

    assign INTERRUPT = IRQ && !instrAddr[31];

    always_comb begin

        // DEFAULT STATES
        REG_WRITE <= 0; // Defaults to blocking register writes
        REG_DEST <= 0;  // Defaults to using rc for write register
        ALU_OP <= 0;    // Defaults to add operation
        ALU_SRC1 <= 0;  // Defaults to using regDataA for ALU
        ALU_SRC2 <= 0;  // Defaults to using regDataB for ALU
        MEM_READ <= 0;  // Defaults to blocking memory reads
        MEM_WRITE <= 0; // Defaults to blocking memory writes
        MEM_REG <= 0;   // Defaults to using ALU output
        NOOP <= 0;      // Defaults flag to off

        if(reset) begin
            // Use default state for reset
        end
        else if(INTERRUPT) begin
            REG_WRITE <= 1;
            REG_DEST <= 3;
            ALU_OP <= 26;
            ALU_SRC1 <= 1;
        end
        else if(opCode == 0) begin
            // R type defaults
            REG_WRITE    <= 1;

            if(funct == 6'b100000)              // ADD
                ALU_OP <= 0;
            else if(funct == 6'b100010)         // SUB
                ALU_OP <= 1;
            else if(funct == 6'b100100)         // AND
                ALU_OP <= 5'b11000;
            else if(funct == 6'b100111)         // NOR
                ALU_OP <= 5'b10001;
            else if(funct == 6'b100101)         // OR
                ALU_OP <= 5'b11110;
            else if(funct == 6'b100110)         // XOR
                ALU_OP <= 5'b10110;
            else if(funct == 6'b000000) begin   // SLL
                ALU_OP   <= 5'b01000;
                ALU_SRC2 <= 1;
            end
            else if(funct == 6'b000010) begin   // SRL
                ALU_OP   <= 5'b01001;
                ALU_SRC2 <= 1;
            end
            else if(funct == 6'b000011) begin   // SRA
                ALU_OP   <= 5'b01011;
                ALU_SRC2 <= 1;
            end
            else if(funct == 6'b101010)         // SLT
                ALU_OP <= 5'b00111;
            else begin
                REG_WRITE <= 1;
                REG_DEST <= 3;
                ALU_OP <= 26;
                ALU_SRC1 <= 1;                
                NOOP <= 1;
            end
        end
        else if(opCode == 6'b001000) begin  // ADDI
            REG_WRITE <= 1;
            REG_DEST <= 1;
            ALU_SRC2 <= 1;
        end
        else if(opCode == 6'b001100) begin  // ANDI
            REG_WRITE <= 1;
            REG_DEST <= 1;
            ALU_OP <= 5'b11000;
            ALU_SRC2 <= 1;
        end
        else if(opCode == 6'b001101) begin  // ORI
            REG_WRITE <= 1;
            REG_DEST <= 1;
            ALU_OP <= 5'b11110;
            ALU_SRC2 <= 1;
        end
        else if(opCode == 6'b001110) begin  // XORI
            REG_WRITE <= 1;
            REG_DEST <= 1;
            ALU_OP <= 5'b10110;
            ALU_SRC2 <= 1;
        end
        else if(opCode == 6'b100011) begin  // LW
            REG_WRITE <= 1;
            REG_DEST <= 1;
            ALU_SRC2 <= 1;
            MEM_READ <= 1;
            MEM_REG <= 1;
        end
        else if(opCode == 6'b101011) begin  // SW
            ALU_SRC2 <= 1;
            MEM_WRITE <= 1;
        end
        else begin // Unknown instr
            NOOP <= 1;
        end
    end
endmodule
