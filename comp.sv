module comp(
	input logic ALUOp3, ALUOp1, z, v, n,
    output logic compout
);

	always_comb begin
		if(ALUOp3 == 0 && ALUOp1 == 0) begin
			compout <= z; //==
		end
		else if(ALUOp3 == 0 && ALUOp1 == 1) begin
			compout <= n != v && (n || v); //<
		end
		else if(ALUOp3 == 1 && ALUOp1 == 0) begin
			compout <= z || (n != v && (n || v)); // <=
		end
		else begin
			compout <= 0;
		end
	end

endmodule