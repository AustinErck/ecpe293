# Script to run testbench

# Compile Design
vlog -sv -work work bool.sv
vlog -sv -work work arith.sv
vlog -sv -work work comp.sv
vlog -sv -work work shift.sv
vlog -sv -work work alu.sv
vlog -sv -work work pc.sv
vlog -sv -work work -suppress 7061 regfile.sv
vlog -sv -work work ctl.sv
vlog -sv -work work beta.sv
	
### ADD YOUR DESIGN FILES HERE FOR COMPILATION ###
vlog -reportprogress 300 -work work stage1.sv
vlog -reportprogress 300 -work work stage2.sv
vlog -reportprogress 300 -work work stage3.sv
vlog -reportprogress 300 -work work stage4.sv
vlog -reportprogress 300 -work work fwd.sv
vlog -sv -work work hazardDetection.sv

# Compile Testbench
vlog -sv -reportprogress 300 -work work tests/imem.sv
vlog -sv -reportprogress 300 -work work tests/dmem.sv
vlog -sv -reportprogress 300 -work work tests/testBeta.sv

# Simulate
vsim -t 1ps -L work -voptargs="+acc" -gtestFileName="tests/testData.txt" -gnumTests=24 testBeta

do tests/opRadix.txt
do tests/funcRadix.txt
do tests/regRadix.txt

# Add waves
add wave -label Clk clk
add wave -label Reset reset
add wave -label IRQ irq
#add wave -radix hex -label ID dutBeta/id
add wave -divider Stage=IF
# Look at signals in IF
add wave -radix hex -label IA_IF ia
add wave -radix OP_LABELS -label OpCodeIF {dutBeta/instrData_s1[31:26]}
# Look at signals in ID
add wave -divider Stage=ID
add wave -radix OP_LABELS -label OpCodeID {dutBeta/instrData_s2[31:26]}
add wave -radix FUNC_LABELS -label FunctID {dutBeta/instrData_s2[5:0]}
add wave -radix REG_LABELS -label RsID {dutBeta/instrData_s2[25:21]}
add wave -radix REG_LABELS -label RtID {dutBeta/instrData_s2[20:16]}
add wave -radix REG_LABELS -label RdID {dutBeta/instrData_s2[15:11]}
# Look at signals in EX
add wave -divider Stage=EX
add wave -radix OP_LABELS -label OpCodeEX {dutBeta/instrData_s3[31:26]}
add wave -radix FUNC_LABELS -label FunctEX {dutBeta/instrData_s3[5:0]}
add wave -radix REG_LABELS -label RsEX {dutBeta/instrData_s3[25:21]}
add wave -radix REG_LABELS -label RtEX {dutBeta/instrData_s3[20:16]}
add wave -radix REG_LABELS -label RdEX {dutBeta/instrData_s3[15:11]}
add wave -radix hex -label MemAddrEX memAddr
# Look at signals in MEM
add wave -divider Stage=MEM
add wave -radix OP_LABELS -label OpCodeMEM {dutBeta/instrData_s4[31:26]}
add wave -radix FUNC_LABELS -label FunctMEM {dutBeta/instrData_s4[5:0]}
add wave -radix REG_LABELS -label RsMEM {dutBeta/instrData_s4[25:21]}
add wave -radix REG_LABELS -label RtMEM {dutBeta/instrData_s4[20:16]}
add wave -radix REG_LABELS -label RdMEM {dutBeta/instrData_s4[15:11]}
#add wave -radix hex -label MemReadData dutBeta/aluOut_s4
#add wave -radix hex -label MemWriteData dutBeta/regDataB_s4
#add wave -label MemWrite MemWrite
#add wave -label MemRead MemRead

add wave -divider Debug_Signals
add wave dutBeta/STALL
add wave -radix hex *
add wave -divider Debug_Signals2
add wave -radix hex dutBeta/memData_s4
add wave -radix hex dutBeta/STAGE2/registers/registers


#### Add your debug signals here ####

# Plot signal values
view structure
view signals
run 10 ns