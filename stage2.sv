module stage2(
	input logic clk, REG_WRITE,
    input logic [1:0] REG_DEST,
    input logic [4:0] regAddrW,
    input logic [31:0] instrData, regDataW,
    output logic [4:0] regAddrA, regAddrB, regAddrC,
    output logic [5:0] opCode, funct,
    output logic [25:0] targetAddress,
    output logic [31:0] regDataA, regDataB, shamtZero, immediateSignExt, immediateZero
);

    /*** Internal Signals ***/

    // Register fields
    logic [4:0] shamt;
    logic [15:0] immediateAddr;

    /*** Signal Assignment ***/

    // General instr fields
    assign opCode = instrData[31:26];
    assign regAddrA = instrData[25:21];
    assign regAddrB = instrData[20:16];

    // R-type instr fields
    assign regAddrC = instrData[15:11];
    assign shamt = instrData[11:6];
    assign funct = instrData[5:0];

    // I-type instr fields
    assign immediateAddr = instrData[15:0];

    // J-type instr fields
    assign targetAddress = instrData[25:0];

    // Pad immediate values
    assign shamtZero = { {27{0}}, shamt[4:0] };
    assign immediateSignExt = { {16{immediateAddr[15]}}, immediateAddr[15:0] };
    assign immediateZero = { {16{0}}, immediateAddr[15:0] };


    /*** Modules ***/

    regfile registers(
        // Inputs
        .clk(clk),
        .RegWrite(REG_WRITE),
        .RegDst(REG_DEST),
        .ra(regAddrA),
        .rb(regAddrB),
        .rc(regAddrW),
        .wdata(regDataW),
        // Outputs
        .radata(regDataA),
        .rbdata(regDataB)
    );

endmodule