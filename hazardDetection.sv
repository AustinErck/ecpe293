module hazardDetection(
	input logic clk,
	input logic [4:0] regAddrA_s2, regAddrB_s2, regAddrB_s3,
	input logic [5:0] opCode_s2, opCode_s3,
    output logic STALL
);

	logic detectedRAW;
	logic stalled;

	// Stall occurs when ID uses result imm following a LD
	// | X | -> | ADD r3, r1, r2 | -> | LD r1, 5(x) |
	// Therefore it occurs if r1/r2 is loaded (opCode_s2 = 0)
	// OR			occurs if r1 is loaded (opcode_s2 != 0)

	// Determine if a stall is needed
	always_comb begin
        if(opCode_s3 == 6'b100011 && (regAddrA_s2 == regAddrB_s3 || ( opCode_s2 == 0 && regAddrB_s2 == regAddrB_s3))) begin
            detectedRAW <= 1;
        end
        else begin 
            detectedRAW <= 0;
        end
    end

	// Reset stall on pos clock
	always_ff @(negedge clk ) begin

		if(detectedRAW && stalled == 0) begin
			STALL <= 1;
			stalled <= 1;
		end
		else begin
			STALL <= 0;
			stalled <= 0;
		end
	end

endmodule