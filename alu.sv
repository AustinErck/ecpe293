module alu(
	input logic [31:0] A, B,
    input logic [4:0] ALUOp,
    output logic [31:0] Y,
    output logic z, v, n
);

	// Signal declarations
	logic [31:0] boolout, shiftout, arithout;
	logic compout;

	// Module declarations
	bool xbool(ALUOp[3:0], A, B, boolout);
	arith xarith(ALUOp[1:0], A, B, arithout, z, v, n);
	comp xcomp(ALUOp[3], ALUOp[1], z, v, n, compout);
	shift xshift(ALUOp[1:0], A, B, shiftout);

	logic outputIsBool, outputIsArith, outputIsComp;
	assign outputIsBool = ALUOp[4];
	assign outputIsArith = ALUOp <= 1;
	assign outputIsComp = ALUOp[2];

	// Output assignment
	assign Y = outputIsBool ? boolout : (outputIsArith ? arithout : (outputIsComp ? { {31{0}}, compout } : shiftout));

endmodule