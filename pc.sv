module pc(
	input logic clk, reset, IRQ, NOOP, STALL,
    output logic [31:0] instrAddr, nextInstrAddr
);

	// Calculate the next instruction in mem
	assign nextInstrAddr = instrAddr + 4;

    always_ff @(posedge clk or posedge reset) begin
    	if(reset)
    		instrAddr <= 32'h80000000;
		else if(IRQ)
    		instrAddr <= 32'h80000008;
		else if(NOOP)
    		instrAddr <= 32'h80000004;
		else if(STALL)
			instrAddr <= instrAddr; // DO NOTHING
		else begin
			instrAddr <= nextInstrAddr;
		end
    end

endmodule